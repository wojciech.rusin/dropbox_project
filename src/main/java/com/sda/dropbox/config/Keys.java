package com.sda.dropbox.config;

public interface Keys {

    String DROPBOX_KEY = "dropbox.key";
    String DIRECTORY = "directory";
}

package com.sda.dropbox.listener;

import com.sda.dropbox.config.ConfigService;
import com.sda.dropbox.config.Keys;
import com.sda.dropbox.upload.DropBoxUploader;
import com.sda.dropbox.upload.Uploader;

import java.io.IOException;
import java.nio.file.*;

import static com.sda.dropbox.config.Keys.DIRECTORY;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;

public class DirectoryListener {
    private final Uploader uploader;
    private final String dir;

    public DirectoryListener(Uploader uploader, ConfigService cfg) {
        this.uploader = uploader;
        this.dir = cfg.get(DIRECTORY);
    }
//    private static final String DIR = "P:\\test_directory\\";


    public void listen() {
        try {
            WatchService watchService = FileSystems.getDefault().newWatchService();
            Path path = Paths.get(dir);
            path.register(watchService, ENTRY_CREATE); //DELETE and MODIFY
            WatchKey key;
            while ((key = watchService.take()) != null) {
                String name = key.pollEvents().get(0).context().toString(); //context zwraca nazwe pliku
                System.out.println(name);
                uploader.upload(dir + name, name);
                key.reset();
            }
        } catch (IOException e) {
            throw new ListenerException("Can not listen file " + dir, e);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}

